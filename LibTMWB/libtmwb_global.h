#ifndef LIBTMWB_GLOBAL_H
#define LIBTMWB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(LIBTMWB_LIBRARY)
#  define LIBTMWBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define LIBTMWBSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // LIBTMWB_GLOBAL_H
