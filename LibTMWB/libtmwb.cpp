#include "libtmwb.h"

#include <QSerialPort>
#include <QList>
#include <QSerialPortInfo>
#include <QDebug>
#include <QThread>


libTMWB::libTMWB()
{
}

QList<twmbDevice*>* libTMWB::enumerateTWMBDevices()
{
    QList<QSerialPortInfo> lAvailablePorts = QSerialPortInfo::availablePorts();
    QList<twmbDevice*>* devices = new QList<twmbDevice*>;
    for(QSerialPortInfo portInfo : lAvailablePorts) {
        qDebug() << "Found serial port: "<< portInfo.systemLocation();
        QSerialPort* port = new QSerialPort(portInfo);
        port->setBaudRate(115200);
        port->open(QIODevice::ReadWrite);
        QThread::msleep(1000);
        port->clear();
        port->flush();
        QThread::msleep(50);
        QByteArray ba = QString("iP").toUtf8();
        if(!port->isOpen()) {
            qDebug() << "Failed to open port. Maybe an other program is using it";
            delete port;
            continue;
        }
        port->putChar('i');
        port->putChar('P');
        if(!port->waitForBytesWritten()) {
            qDebug() << "Failed to write bytes";
            delete port;
            continue;
        }
        port->flush();
        if(!port->waitForReadyRead(500)) {
            qDebug() << "Not a tmwb device";
            delete port;
            continue;
        }

        QByteArray resp = port->readAll();
        for(auto c : resp) {
            int i = c;
            qDebug() << i;
        }
    }
}
