#include "configwindow.h"
#include <libtmwb.h>
#include <QApplication>

int main(int argc, char *argv[])
{
    libTMWB::enumerateTWMBDevices();
    QApplication a(argc, argv);
    ConfigWindow w;
    w.show();

    return a.exec();
}
