#ifndef LIBTMWB_H
#define LIBTMWB_H

#include "libtmwb_global.h"
#include "twmbdevice.h"

class LIBTMWBSHARED_EXPORT libTMWB
{

public:
    libTMWB();

    static QList<twmbDevice*>* enumerateTWMBDevices();
};



#endif // LIBTMWB_H
