// FastCRC library: https://github.com/FrankBoesing/FastCRC
#include <FastCRC.h>

FastCRC8 CRC8;

//Barrow099 - Do Not Modify
#define DEVICE_VERSION 1
#define SOFTWARE_VERSION 1
#define SOFTWARE_REVISION 2
//End Barrow099 Section


// DONT CHANGE CHANNEL NUMBER
// CRC WONT WORK!

uint8_t DMXchannels = 48;
uint8_t dArr[118] = {0};
uint8_t pos = 0;

// #define ANALOG_INPUT // uncomment if analog in A0 is used as test var input


const long interval = 100;  // in millisecs
unsigned long prevmillis = 0;
unsigned long currmillis;

void connectMode();

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  connectMode();

  
}

void loop() {
  // put your main code here, to run repeatedly:
  currmillis = millis();
  if ( (currmillis-prevmillis) >= interval ){
    prevmillis = currmillis;
    sendData();
  }
}

void sendData() {
  pos = 0;
  uint8_t mt = 1;
  #ifdef ANALOG_INPUT
    mt = analogRead(A0)/4;
  #endif
  
  Serial.write(0x69); //startbyte ;)
  dArr[pos++] = 0x69;
  // DMX length
  Serial.write(DMXchannels);
  dArr[pos++] = DMXchannels;
  Serial.write(255-DMXchannels);
  dArr[pos++] = (255-DMXchannels);
  // DMX data
  for ( int i = 0; i < DMXchannels; i++ ) {
    uint8_t data = mt*i*i;
    Serial.write(data);
    dArr[pos++] = data;
    Serial.write(255-data);
    dArr[pos++] = (255-data);
  }
  // separator bytes
  Serial.write(0x0);
  dArr[pos++] = 0x0;
  Serial.write(0x0);
  dArr[pos++] = 0x0;
  // CONTROL data
  for ( int i = 0; i < 8; i++ ){
    uint8_t data = mt*i*i*i;
    Serial.write(data);
    dArr[pos++] = data;
    Serial.write(255-data);
    dArr[pos++] = (255-data);
  }
  // MAXIM-CRC8
  // MAXIM (poly=0x31 init=0x00 refin=true refout=true xorout=0x00 check=0xa1)
  Serial.write(CRC8.maxim(dArr, 118));
  Serial.flush();
}

//Serial device auto-detect funtianality, device information querrying
/*
 * Send autodetect byte 0x70
 * Waiting for connection byte: 0x69
 * If received, send device info in the following order:
 * 1: Device verion number
 * 2: Software version number
 * 3: Software revision
 * 4: DMX Channel mode (48,96) 
 * 5: Update ferq.
 * 6. END 0x51
 */
 void connectMode() {
  bool conn = false;
  while(!conn) {
    while(!Serial.available());
    int ch = Serial.read();
    if(ch == 0x69) {
      Serial.write(0x69);
      Serial.write(DEVICE_VERSION);
      Serial.write(SOFTWARE_VERSION);
      Serial.write(SOFTWARE_REVISION);
      Serial.write(DMXchannels);
      Serial.write(interval);
      Serial.write(0x51);
      Serial.flush();
      delay(200);
      if(Serial.read() == 0x50)
        conn = true;
    }
  }
}
